﻿//// Copyright (c) Microsoft Corporation. All rights reserved

(function () {
    "use strict";

    //var content;
    var channel;
    var pushNotifications = Windows.Networking.PushNotifications;
    var updateNotifications = Windows.UI.Notifications;
    var channelOperation = pushNotifications.PushNotificationChannelManager.createPushNotificationChannelForApplicationAsync();

    var applicationData = Windows.Storage.ApplicationData.current;
    var localSettings = applicationData.localSettings;

    var shareUrl;
    var shareTitle;

    var hwBack = WinJS.Application;
    
    WinJS.UI.Pages.define("/pages/home/home.html", {
        processed: function (element, option) {
            //WinJS.log && WinJS.log("", "Sample", "status"); /* Clear the log */
            //return WinJS.Binding.processAll(element);

            // Register handler for the "datarequested" event.  This event is fired when the Share charm is used,
            // and the handler will be responsible for creating the data package to be shared.
            Windows.ApplicationModel.DataTransfer.DataTransferManager.getForCurrentView().addEventListener("datarequested", dataRequested);
            Windows.ApplicationModel.DataTransfer.DataTransferManager.getForCurrentView().addEventListener("targetapplicationchosen", onTargetApplicationChosen);
            
            
            updateNotifications.TileUpdateManager.createTileUpdaterForApplication().clear();
            updateNotifications.BadgeUpdateManager.createBadgeUpdaterForApplication().clear();
            updateNotifications.ToastNotificationManager.history.clear();
             
            // Register for navigation-related events in the WebView control.  These don't include
            // navigation events within sub-frames -- there are separate MSWebViewFrame* events for those.
            var webviewControl = document.getElementById("webview");
            webviewControl.addEventListener("MSWebViewNavigationStarting", navigationStarting);
            webviewControl.addEventListener("MSWebViewContentLoading", contentLoading);
            webviewControl.addEventListener("MSWebViewDOMContentLoaded", domContentLoaded);
            webviewControl.addEventListener("MSWebViewNavigationCompleted", navigationCompleted);
            webviewControl.addEventListener("MSWebViewUnviewableContentIdentified", unviewableContentIdentified);

            webviewControl.navigate(pageUrl);
            
            // handle the hardware back button
            // navigate webview back if possible, otherwise let HW do its thing
            hwBack.onbackclick = function (e) {
                if (webviewControl.canGoBack) {
                    webviewControl.goBack();            
                    return true;
                }
            };           

            // try network again button (in case we weren't able to load page)           
            document.getElementById("tryagainButton").addEventListener("click", tryNetworkAgain, false);

            // script notify
            webviewControl.addEventListener("MSWebViewScriptNotify", scriptNotify);

            // event listeners for buttons only used in debug mode (sharing, pick contacts)
            //document.getElementById("shareButton").addEventListener("click", showShareUI, false);
            //document.getElementById("pickContacts").addEventListener("click", pickContacts, false);
        }
    });


    function tryNetworkAgain() {

        document.getElementById("tryagainButton").style.display = "none";
        try {
            document.getElementById("webview").navigate(productionUrl);
        } catch (error) {
            WinJS.log && WinJS.log("\"" + destinationUrl + "\" is not a valid absolute URL.\n", "sdksample", "error");
            return;
        }

    }
    //****************************************************************************
    //
    // Script Notify section - getting input from the web app
    //
    //****************************************************************************
    function scriptNotify(e) {
        
        var jsonStr = JSON.parse(e.value);
        var action = jsonStr['wpAction'];

        switch (action) {
            case "SharePost":   // share a post
                shareUrl = jsonStr['uri'];
                shareTitle = jsonStr['postTitle'];
                showShareUI();
                break;
            case "InviteFriends":   // initiate contact picker
                pickContacts();
                break;
            case "FirstPageAfterLogin":
                 startPush();    // only register for push notifications after the user has logged in
                break;
            default:
                break;
        }
    }

    //****************************************************************************
    //
    // Pick Contacts
    //
    //****************************************************************************
    // Pick one or more contacts.
    function pickContacts() {        

        // Create the picker
        var picker = new Windows.ApplicationModel.Contacts.ContactPicker();        
        picker.desiredFieldsWithContactFieldType.append(Windows.ApplicationModel.Contacts.ContactFieldType.email);

        // Open the picker for the user to select contacts
        picker.pickContactsAsync().done(function (contacts) {
            if (contacts.length > 0) {

                //var mailtoString = "mailto:?BCC=";

                var firstLoop = true;
                var sendToList;
                contacts.forEach(function (contact) {
                    
                    if (!firstLoop) {
                        sendToList += ","
                    } else {
                        firstLoop = false; 
                    }
                    sendToList += contact.emails[0].address;
                });

                var subject = "Invitation to try Subcurrent";
                var body = "\" I'm sending you this invitation to try Subcurrent, a place where you can anonymously discuss, view, and vote on company related" +
                            " topics. Subcurrent is a closed network limited to employees from your company and all content is generated by your fellow" + 
                            "co-workers.\n\nCurrently available on the web and the Windows Phone Store.\nhttp://getsubcurrent.com/download";

                var mailtoStr = "mailto:?bcc=" + encodeURIComponent(sendToList) + "&subject=" + encodeURIComponent(subject) + "&body=" + encodeURIComponent(body);
                var uri = new Windows.Foundation.Uri(mailtoString);

                Windows.System.Launcher.launchUriAsync(uri).then(
                    function (success) {
                        if (success) {
                            // URI launched
                        } else {
                            // URI failed to launch
                        }
                    });

            } else {
                // The picker was dismissed without selecting any contacts
                WinJS.log && WinJS.log("No contacts were selected", "sample", "status");
            }
        });
    }

    //****************************************************************************
    //
    // Handle Notification Section
    //
    //****************************************************************************

    function updateServerNotificationChannel(strServerChannel) {
        // updates the web client with udpated channel

        if (strServerChannel) {
            var asyncOp = document.getElementById("webview").invokeScriptAsync("updateWPChannel", strServerChannel);
            asyncOp.oncomplete = function (e) {

            }
        }
        asyncOp.start();        
    }

    function alertWebClientWithApp() {
        // alerts the web client that this is a WP mobile app

        var asyncOp = document.getElementById("webview").invokeScriptAsync("updateWPAgent");
        asyncOp.oncomplete = function (e) {

        }

        asyncOp.start();
    }

    function startPush() {

        // check if user has disabled push notifications 


        // if push not disabled
        return channelOperation.then(function (newChannel) {
             channel = newChannel;
            channel.addEventListener("pushnotificationreceived", onPushNotification, false);

            // check if this is the same channel that we have on file
            // if yes, then do nothing
            // if no, update the server with new channel info
            // check if this is the same channel that we have on file
            var savedChannel = localSettings.values["savedChannel"];

            if (!savedChannel || savedChannel != newChannel.uri) {

                // need to write to updated channel to local file
                localSettings.values["savedChannel"] = newChannel.uri;

                // need to update the server with the new channel
                updateServerNotificationChannel(newChannel.uri);
            }
            
        },
                function (error) {
                    //fail - bubble up notification error somehow
                }
        );
    }

    // handle case where app is in foreground, so we don't want notification to disturb user
    function onPushNotification(e) {
        var notificationPayload;

        switch (e.notificationType) {
            case pushNotifications.PushNotificationType.toast:
                notificationPayload = e.toastNotification.content.getXml();
                break;

            case pushNotifications.PushNotificationType.tile:
                notificationPayload = e.tileNotification.content.getXml();
                break;

            case pushNotifications.PushNotificationType.badge:
                notificationPayload = e.badgeNotification.content.getXml();
                break;

            case pushNotifications.PushNotificationType.raw:
                notificationPayload = e.rawNotification.content;
                break;
        }

       e.cancel = true;
    }

    //****************************************************************************
    //
    // Sharing events/actions/controls
    //
    //****************************************************************************
    
    function showShareUI() {
        Windows.ApplicationModel.DataTransfer.DataTransferManager.showShareUI();
    }
    function dataRequested(e) {
        var dataPackage = e.request;
        var webviewControl = document.getElementById("webview");

        // Set the data package's properties.  These are displayed within the Share UI
        // to give the user an indication of what is being shared.  They can also be
        // used by target apps to determine the source of the data.
        dataPackage.data.properties.title = shareTitle;
        dataPackage.data.properties.description = "An anonymous polling company-internal network";
        //dataPackage.properties.applicationName = "Subcurrent";

        // Set the data being shared.  Here we share the WebView's current URL and an
        // image of the WebView's current rendered page.
        dataPackage.data.setText(shareUrl + "\n\nI wanted to share this post/poll with you on Subcurrent. Subcurrent is a place where employees can anonymously vote and discuss on company related issues."); 
        //dataPackage.setUri(new Windows.Foundation.Uri(webviewControl.src));

        /*
        var captureOperation = webviewControl.capturePreviewToBlobAsync();

        captureOperation.oncomplete = function (completeEvent) {
            var bitmapStream = Windows.Storage.Streams.RandomAccessStreamReference.createFromStream(completeEvent.target.result.msDetachStream());
            dataPackage.setBitmap(bitmapStream);
        };
        captureOperation.start();
        */
    }

    // this function is called once the user selects their target app for sharing
    function onTargetApplicationChosen(eventArgs) {
        // do nothing for now
    }

    //****************************************************************************
    //
    // Web view events/actions/controls
    //
    //****************************************************************************
    
    function goToUrl() {
        var destinationUrl = pageUrl;   // right now goes to fixed URL
        try {
            document.getElementById("webview").navigate(destinationUrl);
        } catch (error) {
            WinJS.log && WinJS.log("\"" + destinationUrl + "\" is not a valid absolute URL.\n", "sdksample", "error");
            return;
        }
    }

    
    function stopNavigation() {
        document.getElementById("webview").stop();
        updateNavigatingState(false);
    }

    function goForward() {
        var webviewControl = document.getElementById("webview");
        if (webviewControl.canGoForward) {
            webviewControl.goForward();
        }
    }

    // This function is called when navigation either starts or stops in the WebView.
    // This updates the UI, shows/hides the spinning progress indicator
    function updateNavigatingState(isNavigating) {
        document.getElementById("progressRing").style.display = (isNavigating ? "inline" : "none");
        document.getElementById("progressLoading").style.display = (isNavigating ? "inline" : "none");
    }

    // NavigationStarting event is triggered when the WebView begins navigating to a new URL.
    function navigationStarting(e) {
        // clear any previous error messages that were displayed with WinJS.log
        WinJS.log && WinJS.log("", "sdksample", "status");

        appendLog && appendLog("Starting navigation to " + e.uri + ".\n");
        //document.getElementById("urlField").value = e.uri;  // don't need to update filed
        updateNavigatingState(true);
    }

    function contentLoading(e) {
        appendLog && appendLog("Loading content for " + e.uri + ".\n");
    }

    function domContentLoaded(e) {
        appendLog && appendLog("Content for " + e.uri + " has finished loading.\n");
    }

    // NavigationCompleted event is triggered either after all the DOM content has been loaded
    // successfully, or when loading failed.  The event arg for this is different from the other
    // navigation events, and includes a isSuccess field to indicate the status.
    function navigationCompleted(e) {
        updateNavigatingState(false);        

        if (e. isSuccess) {                        

            webview.style.display = "inline";   // ensure we're showing webview every time page is loaded
            document.getElementById("tryagainButton").style.display = "none";

            // let the web app know that we are a WP Mobile App
            alertWebClientWithApp();

            appendLog && appendLog("Navigation completed successfully.\n");

            // now that the page has loaded, let's send the app a renewed notification token 

        } else {
            WinJS.log && WinJS.log("Navigation failed with error code " + e.webErrorStatus, "sdksample", "error");
            webview.style.display = "none";   // ensure we're showing webview every time page is loaded
            document.getElementById("tryagainButton").style.display = "inline";            
        }
    }

    // UnviewableContentIdentified event is triggered when the URL being navigated to is not
    // a type that can be displayed in WebView, for example an EXE file or a ZIP file.
    function unviewableContentIdentified(e) {
        updateNavigatingState(false);
        WinJS.log && WinJS.log(e.uri + " cannot be displayed in WebView", "sdksample", "error");
    }

    // Appends a line of text to logArea
    function appendLog(message) {
        // we should add some kind of logging here
    } 
    

})();